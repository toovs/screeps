// example declaration file - remove these and add your own custom typings

// memory extension samples
interface CreepMemory {
  [name: string]: any
}

interface Memory {
  uuid: number;
  log: any;
}

// `global` extension samples
declare namespace NodeJS {
  interface Global {
    log: any;
  }
}

declare namespace _ {
}
