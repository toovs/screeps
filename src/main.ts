import _ from 'underscore'

import Spawn from "Spawn"
import Upgrader from "Creepers/Upgrader"
import Constructor from "Creepers/Constructor"
import Transporter from "Creepers/Transporter"

const memory = JSON.parse(RawMemory.get())

if (memory['creeps'] == undefined) { memory['creeps'] = {} }

for (const name in memory['creeps']) {
  if (!Game.creeps[name]) {
    delete memory['creeps'][name]
  }
}

for(const name in Game.spawns) {
  let spawn = new Spawn(memory, Game.spawns[name])
  spawn.createCreep()
}

for (const name in Game.creeps) {
  let creep
  let creepMemory = memory['creeps'][name]
  switch (creepMemory.role) {
    case 'upgrader': { creep = new Upgrader(memory, Game.creeps[name]); break }
    case 'constructor': { creep = new Constructor(memory, Game.creeps[name]); break }
    case 'transporter': { creep = new Transporter(memory, Game.creeps[name]); break }
    //case 'harvester': { creep = new Harvester(memory, Game.creeps[name]); break }
  }
  if (creep) { creep.run() }
}

RawMemory.set(JSON.stringify(memory))
