import ScreepObject from "ScreepObject"

export default class ScreepRoom extends ScreepObject {
  private room: Room

  constructor(memory: any, room: Room) {
    super(memory)
    this.room = room
  }

  initRoom(): void {
    if (!this.memory['rooms'][this.room.id]) {
      this.memory['rooms'][this.room.name] = { 'creeps': [] }
    }
  }
}
